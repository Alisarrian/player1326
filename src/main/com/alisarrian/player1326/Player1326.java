package com.alisarrian.player1326;

import com.alisarrian.player1326.state.NoWins;
import com.alisarrian.player1326.state.State;

public class Player1326 implements State {

    private State state = new NoWins();

    public Player1326() {
    }

    @Override
    public int getBetMultiplier() {
        return this.state.getBetMultiplier();
    }

    @Override
    public void nextWin(Player1326 player) {
        System.out.println("Winner!");
        this.state.nextWin(this);
    }

    @Override
    public void nextLose(Player1326 player) {
        System.out.println("Loser!");
        this.state.nextLose(this);
    }

    public void placeBet() {
        String sb = "Current state: " + state.getClass().getSimpleName() +
                "\nBet multiplier: " + this.getBetMultiplier() +
                "\n";
        System.out.println(sb);
    }

    public void setState(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }
}
