package com.alisarrian.player1326;

public class P1326Main {
    public static void main(String[] args) {

        Player1326 player = new Player1326();

        player.placeBet();
        player.nextWin(player);

        player.placeBet();
        player.nextWin(player);

        player.placeBet();
        player.nextWin(player);

        player.placeBet();
        player.nextWin(player);

        player.placeBet();
        player.nextWin(player);

        player.placeBet();
        player.nextLose(player);

    }
}
