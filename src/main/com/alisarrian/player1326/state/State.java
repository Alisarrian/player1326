package com.alisarrian.player1326.state;

import com.alisarrian.player1326.Player1326;

public interface State {
    int getBetMultiplier();
    void nextWin(Player1326 player);
    void nextLose(Player1326 player);
}
