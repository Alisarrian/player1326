package com.alisarrian.player1326.state;

import com.alisarrian.player1326.Player1326;

public class NoWins implements State {
    @Override
    public int getBetMultiplier() {
        return 1;
    }

    @Override
    public void nextWin(Player1326 player) {
        player.setState(new OneWin());
    }

    @Override
    public void nextLose(Player1326 player) {
        player.setState(this);
    }
}
