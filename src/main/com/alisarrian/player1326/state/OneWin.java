package com.alisarrian.player1326.state;

import com.alisarrian.player1326.Player1326;

public class OneWin implements State {
    @Override
    public int getBetMultiplier() {
        return 3;
    }

    @Override
    public void nextWin(Player1326 player) {
        player.setState(new TwoWins());
    }

    @Override
    public void nextLose(Player1326 player) {
        player.setState(new NoWins());
    }
}
