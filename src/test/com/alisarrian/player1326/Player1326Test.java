package com.alisarrian.player1326;

import com.alisarrian.player1326.state.NoWins;
import com.alisarrian.player1326.state.OneWin;
import com.alisarrian.player1326.state.ThreeWins;
import com.alisarrian.player1326.state.TwoWins;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.jupiter.api.Assertions.*;

class Player1326Test {
    private static final int NO_WINS_MULTIPLIER = 1;
    private static final int ONE_WIN_MULTIPLIER = 3;
    private static final int TWO_WINS_MULTIPLIER = 2;
    private static final int THREE_WINS_MULTIPLIER = 6;

    private Player1326 player1326;

    @BeforeEach
    void setUp() {
        player1326 = new Player1326();
    }

    @Test
    void givenNewPlayer1326_whenNoPlaying_thenStateNoWins() {
        assertThat(player1326.getState(), instanceOf(NoWins.class));
        assertEquals(NO_WINS_MULTIPLIER, player1326.getBetMultiplier());
    }

    @Test
    void givenNewPlayer1326_whenOneWinInARow_thenStateOneWin() {
        player1326.nextWin(player1326);

        assertThat(player1326.getState(), instanceOf(OneWin.class));
        assertEquals(ONE_WIN_MULTIPLIER, player1326.getBetMultiplier());
    }

    @Test
    void givenNewPlayer1326_whenTwoWinsInARow_thenStateTwoWins() {
        player1326.nextWin(player1326);
        player1326.nextWin(player1326);

        assertThat(player1326.getState(), instanceOf(TwoWins.class));
        assertEquals(TWO_WINS_MULTIPLIER, player1326.getBetMultiplier());
    }

    @Test
    void givenNewPlayer1326_whenThreeWinsInARow_thenStateThreeWins() {
        player1326.nextWin(player1326);
        player1326.nextWin(player1326);
        player1326.nextWin(player1326);

        assertThat(player1326.getState(), instanceOf(ThreeWins.class));
        assertEquals(THREE_WINS_MULTIPLIER, player1326.getBetMultiplier());
    }

    @Test
    void givenNewPlayer1326_whenFourWinsInARow_thenStateNoWins() {
        player1326.nextWin(player1326);
        player1326.nextWin(player1326);
        player1326.nextWin(player1326);
        player1326.nextWin(player1326);

        assertThat(player1326.getState(), instanceOf(NoWins.class));
        assertEquals(NO_WINS_MULTIPLIER, player1326.getBetMultiplier());
    }

    @Test
    void givenNewPlayer1326_whenLose_thenStateNoWins() {
        player1326.nextLose(player1326);

        assertThat(player1326.getState(), instanceOf(NoWins.class));
        assertEquals(NO_WINS_MULTIPLIER, player1326.getBetMultiplier());
    }

    @Test
    void givenNewPlayer1326_whenWinAndLose_thenStateNoWins() {
        player1326.nextWin(player1326);
        player1326.nextLose(player1326);

        assertThat(player1326.getState(), instanceOf(NoWins.class));
        assertEquals(NO_WINS_MULTIPLIER, player1326.getBetMultiplier());
    }

    @Test
    void givenNewPlayer1326_whenDoubleWinAndLose_thenStateNoWins() {
        player1326.nextWin(player1326);
        player1326.nextWin(player1326);
        player1326.nextLose(player1326);

        assertThat(player1326.getState(), instanceOf(NoWins.class));
        assertEquals(NO_WINS_MULTIPLIER, player1326.getBetMultiplier());
    }

    @Test
    void givenNewPlayer1326_whenTripleWinAndLose_thenStateNoWins() {
        player1326.nextWin(player1326);
        player1326.nextWin(player1326);
        player1326.nextWin(player1326);
        player1326.nextLose(player1326);

        assertThat(player1326.getState(), instanceOf(NoWins.class));
        assertEquals(NO_WINS_MULTIPLIER, player1326.getBetMultiplier());
    }
}